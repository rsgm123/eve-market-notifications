from datetime import datetime
from uuid import uuid4

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models


class Character(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.TextField()

    user = models.ManyToManyField('auth.User')

    def __str__(self):
        return '[Character] {} - {}'.format(self.id, self.name)


class CharacterSettings(models.Model):
    character = models.OneToOneField(
        'Character',
        primary_key=True,
        related_name='settings',
        on_delete=models.CASCADE,
    )

    eve_mail_alerts = models.BooleanField(default=False)
    eve_mail_recipient = models.ForeignKey(
        'Character',
        on_delete=models.CASCADE
    )


class RefreshToken(models.Model):
    character = models.OneToOneField(
        'Character',
        primary_key=True,
        related_name='refresh_token',
        on_delete=models.CASCADE,
    )

    refresh_token = models.CharField(max_length=150)
    token_type = models.CharField(max_length=60)  # probably bearer, could be more later
    revoked_permission = models.BooleanField()

    scopes = models.TextField()
    character_for = models.CharField(max_length=60)  # character/corporation?/other?
    character_owner_hash = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)  # date time of access token creation

    def __str__(self):
        return '[RefreshToken] {}[{}] - {}'.format(self.character.name, self.character.id, self.created_date)

    def get_scopes(self):
        return self.scopes.split(' ')


class AccessToken(models.Model):
    refresh_token = models.OneToOneField(
        'RefreshToken',
        primary_key=True,
        related_name='access_token',
        on_delete=models.CASCADE
    )

    access_token = models.CharField(max_length=300)

    expires_in = models.IntegerField()
    created_date = models.DateTimeField(auto_now=True)  # date time of access token creation

    def __str__(self):
        # this may make multiple calls, don't get strings in views, this is meant for the admin page
        return '[AccessToken] {}[{}] - {}'.format(self.refresh_token.character.name, self.refresh_token.character.id,
                                                  self.created_date)


class Item(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=120)


class Region(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=80)


class Location(models.Model):
    id = models.BigIntegerField(primary_key=True)
    region = models.ForeignKey('Region', blank=True, null=True, on_delete=models.CASCADE)

    name = models.CharField(max_length=200, blank=True, null=True)
    last_update = models.DateTimeField(default=datetime.now)

    is_structure = models.BooleanField()


class Market(models.Model):
    """
    Represents the market for an item at a given location.
    Market orders' location and item type are closely related.
    Similarly to how you cannot compete with orders in different locations,
    you can't compete for orders of a different type.

    Orders with the same market, or (item, location) pair, are all of the orders that can compete with each other.
    """
    item = models.ForeignKey('Item', on_delete=models.CASCADE)
    location = models.ForeignKey('Location', on_delete=models.CASCADE)

    best_buy_order = models.ForeignKey('Order', blank=True, null=True, related_name='+', on_delete=models.CASCADE)
    best_sell_order = models.ForeignKey('Order', blank=True, null=True, related_name='+', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('item', 'location')


class Order(models.Model):
    order_id = models.BigIntegerField(primary_key=True)  # 123,
    character = models.ForeignKey('Character', blank=True, null=True, on_delete=models.CASCADE)
    market = models.ForeignKey('Market', on_delete=models.CASCADE)

    # time
    issued = models.DateTimeField()  # "2016-09-03T05:12:25Z",
    closed_date = models.DateTimeField(blank=True, null=True)

    # stats
    duration = models.IntegerField()  # 30,
    is_buy_order = models.BooleanField()  # true,
    volume_remain = models.IntegerField()  # 4422,
    volume_total = models.IntegerField()  # 123456
    min_volume = models.IntegerField()  # 123,
    price = models.DecimalField(max_digits=16, decimal_places=2, db_index=True)  # 10,000,000,000,000.00 # todo: test index performance
    order_range = models.CharField(max_length=20)  # renamed from range

    # computed stats
    is_open = models.BooleanField(default=True, db_index=True)

    # duration
    # is_buy_order
    # issued
    # location_id
    # min_volume
    # order_id
    # price  # changes
    # range
    # type_id
    # volume_remain  # changes
    # volume_total

    def __str__(self):
        if self.character:
            return '[Order] {} - {}[{}] - {} - {}'.format(
                self.order_id,
                self.character.name,
                self.character.id,
                self.is_open,
                self.market.location.last_update
            )

        return '[Order] {} - {} - {}'.format(
            self.order_id,
            self.is_open,
            self.market.location.last_update
        )


# tracks historical price movements, calculated every 1 minute
# does not track order prices, only tracks what things actually traded at
class MarketUpdate(models.Model):
    market = models.ForeignKey(
        'Market',
        on_delete=models.CASCADE,
    )
    created_date = models.DateTimeField(auto_now_add=True)

    # average high/low at period closing
    close_price = models.DecimalField(max_digits=16, decimal_places=2, default=0)

    # number of market transactions
    # (more of a minimum bound due to how esi order updates are in 5 minute batches)
    buy_trade_count = models.IntegerField(default=0)
    sell_trade_count = models.IntegerField(default=0)

    # total period high/low
    high_price = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    low_price = models.DecimalField(max_digits=16, decimal_places=2, default=0)

    # total period volumes
    buy_volume = models.IntegerField(default=0)
    sell_volume = models.IntegerField(default=0)

    # total (exact) price*volumes
    buy_value = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    sell_value = models.DecimalField(max_digits=16, decimal_places=2, default=0)

    # todo: see if any of these are meaningless and remove them late, default=0r
    # avg prices by mean
    buy_mean = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    sell_mean = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    total_mean = models.DecimalField(max_digits=16, decimal_places=2, default=0)

    # avg price by median accounting for transaction volumes
    buy_median = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    sell_median = models.DecimalField(max_digits=16, decimal_places=2, default=0)
    total_median = models.DecimalField(max_digits=16, decimal_places=2, default=0)


class OrderSettings(models.Model):
    order = models.OneToOneField(
        'Order',
        primary_key=True,
        related_name='settings',
        on_delete=models.CASCADE,
    )

    volume_alerts = models.BooleanField(default=False)
    volume_alert_happened = models.BooleanField(default=False)
    volume_limit = models.IntegerField(default=0)

    expiry_alerts = models.BooleanField(default=False)
    expiry_alert_happened = models.BooleanField(default=False)
    expiry_limit = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        # don't allow volumes larger than total volume
        if self.volume_limit > self.order.volume_total:
            self.volume_limit = self.order.volume_total

        if self.expiry_limit > self.order.duration:
            self.expiry_limit = self.order.duration

        super(OrderSettings, self).save(*args, **kwargs)  # save alert


class Alert(models.Model):
    CLOSED = 'closed'
    EXPIRED = 'expired'
    PRICE = 'price'
    VOLUME = 'volume'
    TIME = 'time'
    ORDER_STATES = {
        # eve states
        CLOSED: 'Your order has been closed',
        EXPIRED: 'Your order has expired',
        PRICE: 'The price of your order has been beat',
        VOLUME: 'The quantity remaining on your order has fallen below the limit you set',
        TIME: 'The time remaining on your order has fallen below the limit you set',
    }

    order = models.ForeignKey('Order', related_name='alert_to', on_delete=models.CASCADE)
    change = models.CharField(max_length=30, choices=ORDER_STATES.items())

    cleared = models.BooleanField(default=False)
    evemailed = models.BooleanField(default=False)

    created_date = models.DateTimeField(auto_now_add=True)


# def __str__(self):
#         return '{} - {}'.format(self.pk, self.last_refresh)


class EvemailUnsubscribeToken(models.Model):
    token = models.UUIDField(
        default=uuid4,
        primary_key=True,
    )

    character = models.ForeignKey(
        'Character',
        related_name='+',
        on_delete=models.CASCADE,
    )

    def get_link(self):
        return '{}/unsubscribe/{}/{}/'.format(settings.DOMAIN, self.character.pk, self.token)

# admin.site.register(RefreshToken)
# admin.site.register(AccessToken)
# admin.site.register(Character)
# admin.site.register(CharacterSettings)
# admin.site.register(Order)
# admin.site.register(OrderSettings)
# admin.site.register(Alert)
