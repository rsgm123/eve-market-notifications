import datetime
import json
import logging
import time
import uuid
from collections import defaultdict
from itertools import repeat

import statistics
from django.conf import settings
from django.db.models import Subquery, OuterRef, Exists
from django.db.models.expressions import F
from django.db.models.query_utils import Q
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from eveapi.esi.client import ESI
from huey.exceptions import DataStoreTimeout
from redis.client import StrictRedis

from shekel.huey import auth_refresh
from shekel.models import Character, Order, OrderSettings, Location, Region, Item, Alert, Market, MarketUpdate
from shekel.util import chunks, dict_chunks
from . import GREENLET_QUEUE, THREAD_QUEUE

logger = logging.getLogger(__name__)
redis = StrictRedis(connection_pool=settings.REDIS_POOL)

ESI_CLIENT = ESI(user_agent=settings.ESI_USER_AGENT, auth_callback=auth_refresh.get_auth)

BATCH_SIZE = 5000
MAX_STATION_ID = 2147483647

RUNNING_KEY = settings.REDIS_BASE + '.order_updates.fresh_region_updates'
LOCATION_LAST_OPEN_ORDERS_KEY = settings.REDIS_BASE + '.order_updates.location_open_orders.'
MARKET_TRADE_SET_KEY = settings.REDIS_BASE + '.market_updates.keys'
MARKET_TRADES_BASE_KEY = settings.REDIS_BASE + '.market_updates'

SYNCHRONOUS_STARTUP = False


# periodic tasks (sometimes called runner tasks)
# ----------------------------------------------------------------------------------

@GREENLET_QUEUE.db_periodic_task(settings.CLEAR_GREENLET_LOCKS, name='redis_tasks.clear_greenlet_locks.periodic')
@GREENLET_QUEUE.db_task(name='redis_tasks.clear_greenlet_locks.task')
def clear_greenlet_locks():
    GREENLET_QUEUE.flush_locks()
    logger.info('flushing locks')
