from django.conf import settings

GREENLET_QUEUE = settings.HUEY_GREENLET['huey']  # used for quick, io bound, tasks
THREAD_QUEUE = settings.HUEY_THREAD['huey']  # used for long running cpu/memory intense tasks
