import logging
from functools import partial

import json

from collections import defaultdict
from django.conf import settings
from django.template.loader import get_template
from eveapi.esi.client import ESI

from shekel.huey import auth_refresh
from shekel.models import EvemailUnsubscribeToken, Alert

logger = logging.getLogger(__name__)


# @djhuey.db_task(name='insights.timezones.periodic')
# def timezones():