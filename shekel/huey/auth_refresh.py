import datetime
import logging
import uuid
import time
from django.conf import settings
from django.utils import timezone
from eveapi.esi.errors import ESIPermissionRevoked
from eveapi.sso import refresh
from redis.client import StrictRedis

from shekel.models import AccessToken

from . import GREENLET_QUEUE, THREAD_QUEUE

logger = logging.getLogger(__name__)
redis = StrictRedis(connection_pool=settings.REDIS_POOL)


@GREENLET_QUEUE.db_task(retries=3, retry_delay=5, name='auth_refresh.auth_refresh')  # this could take a while
def auth_refresh(token_id, force):
    """
    Preform a token refresh.
    This task will only run once, until it completes.
    Any secondary runs should block until the new access token is set.
    """
    logger.info('running eve_shekel.auth_refresh')

    lock_key = settings.REDIS_BASE + '.auth_refresh.' + str(token_id)
    lock_value = str(uuid.uuid4())

    lock_acquired = redis.setnx(lock_key, lock_value)
    if not lock_acquired:
        raise Exception('Already refreshing token: ' + str(token_id))

    # if it takes more than 15 seconds, something got stuck
    redis.expire(lock_key, 15)

    access_token = AccessToken.objects.select_related('refresh_token').get(pk=token_id)
    if force or access_token.created_date < timezone.now() - datetime.timedelta(seconds=access_token.expires_in):
        refresh_token = access_token.refresh_token
        logger.info('refreshing access token for {}'.format(refresh_token.character.pk))

        if refresh_token.revoked_permission:
            logger.error('esi permission revoked')
            raise ESIPermissionRevoked()

        try:
            tokens = refresh(refresh_token.pk, refresh_token.refresh_token)
        except ESIPermissionRevoked:
            logger.error('esi permission revoked')
            refresh_token.revoked_permission = True
            refresh_token.save()
            raise

        logger.info('got new token')

        access_token.access_token = tokens['access_token']
        access_token.expires_in = tokens['expires_in']
        access_token.save()

        refresh_token.refresh_token = tokens['refresh_token']
        refresh_token.save()

        logger.info('saved token')

    # delete the lock if the lock is still set from above
    redis_value = redis.get(lock_key)
    if redis_value and redis_value.decode("utf-8") == lock_value:
        redis.delete(lock_key)

    return access_token.access_token


def get_auth(character, force=False):
    """
    Get formatted auth header with access token.
    This checks for expired access tokens and refresh them.
    """
    logger.info('fetching access token', extra={'character': character})
    access_token = AccessToken.objects.select_related('refresh_token').get(refresh_token__character__id=character)
    token_type = access_token.refresh_token.token_type  # probably won't be changing
    token = access_token.access_token

    logger.debug('get_auth callback - {}'.format(access_token.refresh_token))

    if force or access_token.created_date < timezone.now() - datetime.timedelta(seconds=access_token.expires_in):
        start_time = time.time()
        token = auth_refresh(access_token.pk, force).get(blocking=True, timeout=30)
        logger.info('Refreshed token', extra={'duration': time.time() - start_time})

    logger.debug('returning formatted auth header')

    return '{} {}'.format(token_type, token)
