# Generated by Django 2.2.1 on 2019-05-25 05:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shekel', '0017_auto_20190523_1444'),
    ]

    operations = [
        migrations.AddField(
            model_name='marketupdate',
            name='buy_trade_count',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='marketupdate',
            name='sell_trade_count',
            field=models.IntegerField(default=0),
        ),
    ]
