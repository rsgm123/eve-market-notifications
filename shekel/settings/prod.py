from redis import ConnectionPool

from shekel.util import Queue
from .common import *  # noqa

SECRET_KEY = env("DJANGO_SECRET_KEY")

ALLOWED_HOSTS = ['www.eve-shekel.com']

# ESI - PROD keys
CLIENT_ID = env("ESI_CLIENT_ID")
CLIENT_SECRET = env("ESI_CLIENT_SECRET")

ESI_USER_AGENT = 'eve-shekel.com PRODUCTION (by Rsgm Vaille, rsgm.eve@gmail.com)'
DOMAIN = 'https://www.eve-shekel.com'

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'rdb',
        'USER': env('DB_USERNAME'),
        'PASSWORD': env('DB_PASSWORD'),
        'HOST': env('DB_HOST'),
        'PORT': env('DB_PORT'),
    }
}

REDIS_POOL = ConnectionPool(
    host=env('REDIS_PORT_6379_TCP_ADDR'),
    port=env('REDIS_PORT_6379_TCP_PORT'),
    max_connections=1000,
)

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [(env('REDIS_PORT_6379_TCP_ADDR'), env('REDIS_PORT_6379_TCP_PORT'))],
        },
    },
}

REST_FRAMEWORK = {
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
    ),

    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'shekel.authentication.CharacterAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),

    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),

    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.ScopedRateThrottle',
    ),

    'DEFAULT_THROTTLE_RATES': {
        'direct_esi_call': '1/second',
    }
}

# HUEY QUEUES
HUEY_THREAD = {
    'huey': Queue(
        name='shekel.huey.thread',
        blocking=False,
        result_store=True,
        store_errors=False,
        global_registry=False,
        connection_pool=REDIS_POOL,
    ),
    'consumer': {
        'logfile': '/dev/null',
        'workers': 3,
        'worker_type': 'process',
        'backoff': 2,  # Exponential backoff using this rate, -b.
        'max_delay': 20.0,  # Max possible polling interval, -m.
        'periodic': False,  # Enable crontab feature.
        'check_worker_health': True,  # Enable worker health checks.
        'health_check_interval': 1,  # Check worker health every second.
    },
}
HUEY_GREENLET = {
    'huey': Queue(
        name='shekel.huey.greenlet',
        blocking=False,
        result_store=True,
        store_errors=False,
        global_registry=False,
        connection_pool=REDIS_POOL,
    ),
    'consumer': {
        'logfile': '/dev/null',
        'workers': 800,
        'worker_type': 'greenlet',
        'backoff': 2,  # Exponential backoff using this rate, -b.
        'max_delay': 20.0,  # Max possible polling interval, -m.
        'periodic': True,  # Enable crontab feature.
        'check_worker_health': True,  # Enable worker health checks.
        'health_check_interval': 1,  # Check worker health every second.
    },
}
EVEAPI_HUEY_INSTANCE = HUEY_GREENLET['huey']

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} {name:s} {process:d} {processName:s} {threadName:s} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
    },
    # 'filters': {
    #     'special': {
    #         '()': 'project.logging.SpecialFilter',
    #         'foo': 'bar',
    #     },
    #     'require_debug_true': {
    #         '()': 'django.utils.log.RequireDebugTrue',
    #     },
    # },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/django/django.log',
            'formatter': 'verbose',
            'maxBytes': 1024 * 1024 * 10,  # 10 MB
            'backupCount': 10,
        },
        'huey': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/django/huey.log',
            'formatter': 'verbose'
        },
        'logstash': {  # new scaleway elk sever needs the logstash config to be updated
            'level': 'INFO',
            'class': 'logstash.TCPLogstashHandler',
            'host': env('LOGSTASH_URL'),
            'port': env('LOGSTASH_PORT'),  # Default value: 5959
            'version': 1,  # Version of logstash event schema. Default value: 0
            'message_type': 'django',  # 'type' field in logstash message. Default value: 'logstash'.
            'fqdn': False,  # Fully qualified domain name. Default value: false.
            'tags': ['django.request'],  # list of tags. Default: None.
        },
        'huey_logstash': {  # same as above, except WARNING level logging
            'level': 'WARNING',
            'class': 'logstash.TCPLogstashHandler',
            'host': env('LOGSTASH_URL'),
            'port': env('LOGSTASH_PORT'),
            'version': 1,
            'message_type': 'django',
            'fqdn': False,
            'tags': ['django.request'],
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console', 'logfile', 'logstash'],
            'propagate': True,
        },
        'shekel': {
            'handlers': ['console', 'logfile', 'logstash'],
            'level': 'DEBUG',
        },
        'eveapi': {
            'handlers': ['console', 'logfile', 'logstash'],
            'level': 'INFO',
        },
        'channels': {
            'handlers': ['logfile', 'logstash'],
            'level': 'DEBUG',
        },
        'rest_framework': {
            'handlers': ['logfile', 'logstash'],
            'level': 'DEBUG',
        },
        'huey': {
            'handlers': ['logfile', 'huey_logstash'],
            'level': 'INFO',
        },
    }
}
