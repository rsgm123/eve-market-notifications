from collections import Counter
import datetime

from django.core.management.base import BaseCommand
from django.db.models.aggregates import Count
from django.db.models.expressions import Window, F
from django.db.models.functions.datetime import ExtractHour, ExtractWeekDay

from shekel.models import Order, OrderUpdate

DAYS = {
    1: 'Su',
    2: 'M ',
    3: 'Tu',
    4: 'W ',
    5: 'Th',
    6: 'F ',
    7: 'Sa',
}


class Command(BaseCommand):
    help = 'Test analyzing (item,location) timezones'

    def handle(self, *args, **options):
        tmp_orders = Order.objects.order_by('-issued')[:20]
        counts = []
        for tmp_order in tmp_orders:
            item = tmp_order.item
            location = tmp_order.location
            name = '{} - {}'.format(location.name, item.name)
            print('getting all orders for {}'.format(name))
            orders = Order.objects.filter(item=item, location=location)
            order_updates = OrderUpdate.objects.filter(order__in=orders, created_date__gt=datetime.datetime.now() - datetime.timedelta(days=30))
            print('got {} orders and {} order updates'.format(orders.count(), order_updates.count()))
            sum_queryset = order_updates.annotate(
                hour=ExtractHour('created_date'),
                day=ExtractWeekDay('created_date'),
            ).annotate(
                hour_count=Window(
                    expression=Count('pk'),
                    partition_by=[F('hour')],
                ),
                day_count=Window(
                    expression=Count('pk'),
                    partition_by=[F('day')],
                ),
            )
            hour_counts = sum_queryset.order_by('hour').distinct('hour').values_list('hour', 'hour_count')
            day_counts = sum_queryset.order_by('day').distinct('day').values_list('day', 'day_count')
            hours = {hour: 0 for hour in range(24)}
            hours.update({hour: count for hour, count in hour_counts})
            days = {day: 0 for day in DAYS.values()}
            days.update({DAYS[day]: count for day, count in day_counts})
            counts.append((name, hours, days))
            print('finished'.format(orders.count(), order_updates.count()))

        for name, count1, count2 in counts:
            max_1 = max(count1.values())
            max_2 = max(count2.values())
            print('\n\n\n---------  ' + name + '  -----------')
            for hour, value in count1.items():
                print('{:02d}: {}   {}'.format(hour, '#' * int(value / max_1 * 40), value))
            print()
            for day, value in count2.items():
                print('{}: {}   {}'.format(day, '#' * int(value / max_2 * 40), value))
