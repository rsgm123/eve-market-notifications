import itertools

import json
import requests
from django.conf import settings
from django.core.management.base import BaseCommand
from eveapi.esi.client import ESI

from shekel.models import Item

CHUNK_SIZE = 800
ESI_CLIENT = ESI(user_agent=settings.ESI_USER_AGENT)
ESI_CLIENT_NONBLOCKING = ESI(user_agent=settings.ESI_USER_AGENT)


class Command(BaseCommand):
    help = 'Downloads item data from ESI, requires huey running.'

    def handle(self, *args, **options):
        type_ids = ESI_CLIENT.v1.universe.types(fetch_pages=True, cache=False).get(blocking=True, timeout=60)

        # break type_ids into chunks
        type_id_chunks = [type_ids[x:x + CHUNK_SIZE] for x in range(0, len(type_ids), CHUNK_SIZE)]

        print('starting')
        # call the universe.names endpoint (without blocking) on each chunk,
        # then collect all of the running requests into esi_calls
        esi_calls = [ESI_CLIENT.v2.universe.names.post(data=json.dumps(types), blocking=False)
                     for types in type_id_chunks]

        new_items = []
        for esi_call in esi_calls:
            item_types = esi_call.get(timeout=300, blocking=True)

            for item in item_types:
                new_items.append(Item(id=item['id'], name=item['name']))

        # wait for each call to finish, then collect the item name/ids as Item objects
        # new_items = [Item(id=item['id'], name=item['name']) for item in
        #              [items for items in
        #              [esi_call.get(blocking=True) for esi_call in esi_calls]]]

        Item.objects.bulk_create(new_items, ignore_conflicts=True)
        print('finished populating {} items'.format(len(type_ids)))
