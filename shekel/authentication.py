from django.conf import settings
from django.contrib.auth.models import User
from rest_framework import authentication
from uuid import uuid4


class CharacterAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        user = User.objects.filter(pk=request.session.get('user_id')).first()
        if user:
            return user, None

        user = User.objects.create(username=uuid4(), password=uuid4())
        request.session.set_expiry(settings.SESSION_EXPIRY)
        request.session['user_id'] = user.pk

        return user, None
