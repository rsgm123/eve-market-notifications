from functools import wraps

from django.conf import settings
from django.db import close_old_connections
from huey import RedisHuey


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def dict_chunks(d, n):
    """Yield successive n-sized chunks of values from d, along with the keys."""
    key_chunk = []
    values_chunk = []

    for key, values in sorted(d.items(), key=lambda x: len(x[1]), reverse=True):
        key_chunk.append(key)
        values_chunk += values

        if len(values_chunk) >= n:
            yield key_chunk, values_chunk
            key_chunk = []
            values_chunk = []

    yield key_chunk, values_chunk


class Queue(RedisHuey):
    def close_db(self, fn):
        """Decorator to be used with tasks that may operate on the database."""
        @wraps(fn)
        def inner(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            finally:
                close_old_connections()

        return inner

    def db_task(self, *args, **kwargs):
        def decorator(fn):
            ret = self.task(*args, **kwargs)(self.close_db(fn))
            ret.call_local = fn
            return ret

        return decorator

    def db_periodic_task(self, *args, **kwargs):
        def decorator(fn):
            return self.periodic_task(*args, **kwargs)(self.close_db(fn))

        return decorator


def acquire_lock(redis, lock_key, lock_value, expires):
    lock = redis.setnx(lock_key, lock_value)
    if lock:
        redis.expire(lock_key, expires)
    return lock


def delete_lock(redis, lock_key, lock_value):
    # delete the lock if the lock is still set from above
    redis_value = redis.get(lock_key)
    if redis_value and redis_value.decode("utf-8") == lock_value:
        redis.delete(lock_key)
