from functools import partial

import requests
from django.conf import settings
from django_filters.rest_framework.backends import DjangoFilterBackend
from eveapi.esi.client import ESI
from rest_framework import viewsets, serializers, status, mixins
from rest_framework.response import Response
from rest_framework.views import APIView

from shekel.huey import auth_refresh
from shekel.models import Order, OrderSettings


class OrderSerializer(serializers.ModelSerializer):
    region = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()
    type_id = serializers.SerializerMethodField()
    type_name = serializers.SerializerMethodField()
    best_price = serializers.SerializerMethodField()
    last_update = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = (
            'order_id',
            'is_open',
            'best_price',
            'price',
            'issued',
            'closed_date',
            'last_update',
            'duration',
            'volume_remain',
            'volume_total',
            'is_buy_order',
            'character',
            'region',
            'location',
            'type_id',
            'type_name',
        )
        read_only_fields = ('__all__',)
        depth = 0

    def get_region(self, obj):
        return obj.market.location.region.name

    def get_location(self, obj):
        return obj.market.location.name

    def get_type_id(self, obj):
        return obj.market.item.id

    def get_type_name(self, obj):
        return obj.market.item.name

    def get_best_price(self, obj):
        best_order_id = obj.market.best_buy_order if obj.is_buy_order else obj.market.best_sell_order
        return obj.order_id == best_order_id

    def get_last_update(self, obj):
        return obj.market.location.last_update


class OrderSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderSettings
        fields = (
            'order',
            'volume_alerts',
            'volume_limit',
            'expiry_alerts',
            'expiry_limit',
        )
        read_only_fields = ('order',)


class OrderViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('character', 'is_open')

    def get_queryset(self):
        return self.queryset.filter(
            character__user=self.request.user,
            market__location__name__isnull=False
        ).select_related(
            'market__location',
            'market__location__region'
        )


class OrderSettingsViewSet(mixins.RetrieveModelMixin,
                           mixins.UpdateModelMixin,
                           viewsets.GenericViewSet):
    serializer_class = OrderSettingsSerializer
    queryset = OrderSettings.objects.all()

    def get_queryset(self):
        return self.queryset.filter(order__character__user=self.request.user)


class OrderWindow(APIView):
    throttle_scope = 'direct_esi_call'

    def post(self, request, **kwargs):
        if 'order_id' not in kwargs:
            return Response('Missing order_id', status=status.HTTP_400_BAD_REQUEST)

        order = Order.objects.select_related('character', 'character__refresh_token').get(
            order_id=kwargs['order_id'],
            character__user=self.request.user,
        )
        character = order.character

        if 'esi-ui.open_window.v1' not in character.refresh_token.scopes:
            return Response(
                "ESI authorization needed. Add '{}' again with the 'esi-ui.open_window.v1' permissions to use.".format(
                    character.name
                ),
                status=status.HTTP_401_UNAUTHORIZED
            )

        response = ESI(
            user_agent=settings.ESI_USER_AGENT,
            auth_callback=auth_refresh.get_auth,
            character=character.id,
        ).v1.ui.openwindow.marketdetails.post(
            type_id=order.market.item.id,
            use_huey=False,
            raise_error=True
        )

        if isinstance(response, requests.Response):
            return Response(
                'ESI Error {}: {}'.format(response.status_code, response.content),
                status=status.HTTP_400_BAD_REQUEST
            )

        return Response(None, status=status.HTTP_204_NO_CONTENT)
