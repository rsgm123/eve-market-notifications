import datetime
from functools import partial
import re

import requests
from django.conf import settings
from django.db.models.aggregates import Min, Sum, Max, Avg
from django.db.models.expressions import Window, F
from django.db.models.fields import DateTimeField, DecimalField, IntegerField
from django.db.models.functions.datetime import Trunc
from django.db.models.functions.window import Lag
from django.db.models.query_utils import Q
from django.http.response import HttpResponseBadRequest
from django_filters.rest_framework.backends import DjangoFilterBackend
from eveapi.esi.client import ESI
from rest_framework import viewsets, serializers, status, mixins
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework import filters
from rest_framework.viewsets import GenericViewSet

from shekel.huey import auth_refresh
from shekel.models import Order, OrderSettings, Market, MarketUpdate, Item

HISTORY_UNIT_MAP = {'m': 'minute', 'h': 'hour', 'd': 'day'}


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('pk', 'name')
        read_only_fields = ('__all__',)
        depth = 0


class HistorySerializer(serializers.Serializer):
    time = serializers.DateTimeField()
    close_price = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)
    open_price = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)

    total_buy_trade_count = serializers.IntegerField()
    total_sell_trade_count = serializers.IntegerField()

    total_high_price = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)
    total_low_price = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)

    total_buy_volume = serializers.IntegerField()
    total_sell_volume = serializers.IntegerField()

    total_buy_value = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)
    total_sell_value = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)
    total_buy_mean = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)
    total_sell_mean = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)
    total_total_mean = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)
    total_buy_median = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)
    total_sell_median = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)
    total_total_median = serializers.DecimalField(max_digits=16, decimal_places=2, coerce_to_string=True)

    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass


class OrderSerializer(serializers.ModelSerializer):
    region = serializers.SerializerMethodField()
    location = serializers.SerializerMethodField()
    type_id = serializers.SerializerMethodField()
    type_name = serializers.SerializerMethodField()
    best_price = serializers.SerializerMethodField()
    last_update = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = (
            'order_id',
            'price',
            'issued',
            'closed_date',
            'last_update',
            'duration',
            'volume_remain',
            'volume_total',
            'is_buy_order',
            'region',
            'location',
            'type_id',
            'type_name',
        )
        read_only_fields = ('__all__',)
        depth = 0

    def get_region(self, obj):
        return obj.market.location.region.name

    def get_location(self, obj):
        return obj.market.location.name

    def get_type_id(self, obj):
        return obj.market.item.id

    def get_type_name(self, obj):
        return obj.market.item.name

    def get_last_update(self, obj):
        return obj.market.location.last_update


class ItemViewSet(mixins.RetrieveModelMixin, GenericViewSet):
    serializer_class = ItemSerializer
    queryset = Item.objects.all()

    filter_backends = (filters.SearchFilter,)
    search_fields = ('name',)


class OrderViewSet(mixins.ListModelMixin, GenericViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()

    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('market', 'market__location',)

    def get_queryset(self):
        return self.queryset.filter(
            is_open=True,
            market__location__name__isnull=False
        ).select_related(
            'market__location',
            'market__location__region'
        )


class HistoryViewSet(APIView):
    def get(self, request, **kwargs):
        region = request.query_params.get('region', None)
        location = request.query_params.get('location', None)
        start = request.query_params.get('start', datetime.datetime.now() - datetime.timedelta(days=30))
        end = request.query_params.get('end', datetime.datetime.now())
        increment = request.query_params.get('increment', 'd')  # minutes, hours, days

        if increment not in HISTORY_UNIT_MAP:
            raise APIException(detail='Increment not in {}'.format(HISTORY_UNIT_MAP.keys()), code=HTTP_400_BAD_REQUEST)

        location_filter = {}
        if location:
            location_filter = {'market__lcoation__pk': location}

        elif region:
            location_filter = {'market__lcoation__region__pk': region}

        market_history = MarketUpdate.objects.filter(
            market__item__pk=kwargs['item_id'],
            created_date__gt=start,
            created_date__lt=end,
            market__location__is_structure=False,  # no free intel
            **location_filter
        ).annotate(
            time=Trunc('created_date', HISTORY_UNIT_MAP[increment], output_field=DateTimeField())
        ).annotate(
            open_price=Window(
                expression=Lag('close_price'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
            total_buy_trade_count=Window(
                expression=Sum('buy_trade_count'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=IntegerField()
            ),
            total_sell_trade_count=Window(
                expression=Sum('sell_trade_count'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=IntegerField()
            ),
            total_high_price=Window(
                expression=Max('high_price'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
            total_low_price=Window(
                expression=Min('low_price'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
            total_buy_volume=Window(
                expression=Sum('buy_volume'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=IntegerField()
            ),
            total_sell_volume=Window(
                expression=Sum('sell_volume'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=IntegerField()
            ),
            total_buy_value=Window(
                expression=Sum('buy_value'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
            total_sell_value=Window(
                expression=Sum('sell_value'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
            total_buy_mean=Window(
                expression=Avg('buy_mean'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
            total_sell_mean=Window(
                expression=Avg('sell_mean'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
            total_total_mean=Window(
                expression=Avg('total_mean'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
            total_buy_median=Window(
                expression=Avg('buy_median'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
            total_sell_median=Window(
                expression=Avg('sell_median'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
            total_total_median=Window(
                expression=Avg('total_median'),
                partition_by=F('time'),
                order_by=F('time').desc(),
                output_field=DecimalField(max_digits=16, decimal_places=2)
            ),
        ).order_by('-time').distinct('time').values(
            'time',
            'close_price',
            'open_price',
            'total_buy_trade_count',
            'total_sell_trade_count',
            'total_high_price',
            'total_low_price',
            'total_buy_volume',
            'total_sell_volume',
            'total_buy_value',
            'total_sell_value',
            'total_buy_mean',
            'total_sell_mean',
            'total_total_mean',
            'total_buy_median',
            'total_sell_median',
            'total_total_median',
        )[:100]

        serializer = HistorySerializer(market_history, many = True)
        return Response(serializer.data)
