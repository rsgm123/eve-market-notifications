from django_filters.rest_framework.backends import DjangoFilterBackend
from rest_framework import mixins
from rest_framework import serializers
from rest_framework.viewsets import GenericViewSet

from shekel.models import Alert
from shekel.views.order import OrderSerializer


class AlertSerializer(serializers.ModelSerializer):
    order = OrderSerializer()
    change_text = serializers.CharField(source='get_change_display')

    class Meta:
        model = Alert
        fields = (
            'id',
            'order',
            'change',
            'change_text',
            'cleared',
        )
        read_only_fields = (  # all but cleared are read only
            'id',
            'order',
            'change',
            'change_text',
        )


class AlertViewSet(mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   mixins.UpdateModelMixin,
                   GenericViewSet):
    serializer_class = AlertSerializer
    queryset = Alert.objects.all()

    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('order__character',)

    def get_queryset(self):
        return self.queryset.select_related('order').filter(
            cleared=False,
            order__character__user=self.request.user,
        )
