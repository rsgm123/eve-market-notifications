import logging
from urllib import parse

from django.conf import settings
from django.http.response import HttpResponseBadRequest
from django.shortcuts import redirect
from django.utils.crypto import get_random_string
from eveapi.sso import authorize, verify
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from shekel.huey import order_updates
from shekel.models import Character, RefreshToken, AccessToken, CharacterSettings

logger = logging.getLogger(__name__)

ESI_SCOPES = [
    {'required': True, 'name': 'esi-markets.read_character_orders.v1',
     'description': '(Required) Allow reading character market orders.'},
    {'required': False, 'name': 'esi-markets.structure_markets.v1',
     'description': 'Allow reading structure market orders. Allows for order updates every 6 minutes in structures, which is normally 30 minutes.'},
    {'required': False, 'name': 'esi-universe.read_structures.v1',
     'description': 'Allow reading structure information. Used to display citadel/structure names.'},
    {'required': False, 'name': 'esi-ui.open_window.v1',
     'description': 'Allow opening windows in-game. Used to open market orders for items.'},
]


class Scopes(APIView):
    def get(self, request, **kwargs):
        state = get_random_string(length=24)
        request.session['SSO_state'] = state  # also check time since

        return Response({'scopes': ESI_SCOPES, 'state': parse.quote(state)})


class CreateCharacterView(APIView):
    def get(self, request, **kwargs):  # change this to an http redirect
        redirect_url = settings.DOMAIN + '/add-character/redirect'
        scopes = request.GET['scopes']
        state = request.GET['state']

        # make sure all required scopes are specified
        if not {scope['name'] for scope in ESI_SCOPES if scope['required']}.issubset(parse.unquote(scopes).split(' ')):
            return HttpResponseBadRequest()

        parameters = '&'.join([
            'response_type=code',
            'redirect_uri=' + parse.quote(redirect_url),
            'client_id=' + parse.quote(settings.CLIENT_ID),
            'state=' + state,
            'scope=' + scopes,
        ])

        return redirect('https://login.eveonline.com/oauth/authorize?' + parameters)

    def post(self, request, **kwargs):
        if 'code' not in request.data:
            return Response('Missing code data', status=status.HTTP_400_BAD_REQUEST)

        if 'state' not in request.data:
            return Response('Missing state data', status=status.HTTP_400_BAD_REQUEST)

        if 'SSO_state' not in request.session:
            return Response('State not set', status=status.HTTP_400_BAD_REQUEST)

        if request.data['state'] != request.session['SSO_state']:
            return Response('Bad state data', status=status.HTTP_400_BAD_REQUEST)

        tokens = authorize(request.data['code'])
        character_info = verify(tokens['access_token'], tokens['token_type'])

        character, _ = Character.objects.update_or_create(  # update if character is on another user
            id=character_info['CharacterID'],
            defaults={
                'name': character_info['CharacterName'],
            }
        )
        request.user.character_set.add(character)
        CharacterSettings.objects.get_or_create(character=character, defaults={'eve_mail_recipient': character})

        refresh_token, _ = RefreshToken.objects.update_or_create(
            character=character,
            defaults={
                'refresh_token': tokens['refresh_token'],
                'token_type': tokens['token_type'],
                'revoked_permission': False,
                'scopes': character_info['Scopes'],
                'character_for': character_info['TokenType'],
                'character_owner_hash': character_info['CharacterOwnerHash'],
            }
        )

        access_token, _ = AccessToken.objects.update_or_create(
            refresh_token=refresh_token,
            defaults={
                'access_token': tokens['access_token'],
                'expires_in': tokens['expires_in'],
            }
        )

        order_updates.add_character_orders(character.pk)

        return Response({
            'id': character.id,
            'name': character.name
        }, status=status.HTTP_201_CREATED)
