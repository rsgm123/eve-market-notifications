- [x] individual order settings
   - [x] limit alerts

- [x] fix alerts
- [x] alert page
- [x] location page
- [] order page

- [] multiple queues, one for greenlets, one for cpu bound tasks

- [] character stats
   - [] item history
   - [] trading history
   - [] average margin
   - [] monthly income
   - [] graphs
- [] item stats
   - [] average margin
   - [] monthly income
   - [] graphs

- [] analytics/insights (*)
    - [] find the best items to trade by region
       - [] top margin
       - [] top volume
       - [] top margin*volume ?
    - [] filters
       - [] region
       - [] buy/sell volume ?
       - [] buy/sell equality
       - [] margin
       - [] price
       - [] covered timezones (**)
       - [] # of orders ?
    - [] currently traded items that are not doing as well

- [] public market item lookup
    - [] api
    - [] frontend


* - This will show the raw, unmodified best items, I do not condone the idea of only showing subsets to spread the load of people rushing for these items
** - track when all orders get updated, plot on timeline
