#!/usr/bin/env bash

ulimit -n 8192 # fix file limit for caddy
caddy -conf caddyfile -email rsgm.eve@gmail.com -agree
