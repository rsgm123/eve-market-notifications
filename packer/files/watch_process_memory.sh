#!/usr/bin/env bash

free -h >> /var/log/django/memory.log

while [ true ]
do
date >> /var/log/django/memory.log
free -h | grep Mem >> /var/log/django/memory.log
sleep 30s
done
