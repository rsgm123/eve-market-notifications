#!/usr/bin/env bash


TMP_DIR=/tmp/eve-shekel
APP_DIR=/home/django/eve-shekel
SSH_DIR=/home/django/.ssh

apt-get update
apt-get install -y python python-dev python3 python3-dev gcc less htop jq git postgresql-client libpq-dev

# setup user
useradd -m django
usermod --shell /bin/bash django

mkdir $TMP_DIR
tar -xf /tmp/eve-shekel-server.tar.gz -C $TMP_DIR

## setup ssh and git
#mkdir $SSH_DIR
#cp $TMP_DIR/packer/files/ssh/* $SSH_DIR
#chown -R django:django $SSH_DIR
#chmod 700 $SSH_DIR
#chmod 600 $SSH_DIR/*
#
#--user=django ssh-keyscan gitlab.com | --user=django tee /home/django/.ssh/known_hosts
#--user=django git clone git@gitlab.com:rsgm-eve-apps/eve-shekel.git $APP_DIR

# add config files not in git
# cp -r will try to put files in DEST/DIR_NAME, so just specify home here
cp -rv $TMP_DIR /home/django

chown -R django:django $APP_DIR
chmod +x $APP_DIR/.env.sh

mkdir /var/log/django
chown django:django /var/log/django


# install dependencies
cd /tmp

wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py

cd $APP_DIR
pip3 install -r requirements.txt -r prod_requirements.txt


# setup systemd services
cp packer/files/*.service /lib/systemd/system/
