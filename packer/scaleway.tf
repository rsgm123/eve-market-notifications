variable "organization_id" {}
variable "access_key" {}
variable "api_token" {}

variable "main_server_image" {
  default = "499af57e-40f4-4ee1-b46d-5609fdb1fcf9"
}
variable "huey_image" {
  default = "fd334829-b7de-4921-843b-329cb9fff98c"
}

provider "scaleway" {
  organization_id = var.organization_id
  access_key = var.access_key
  secret_key = var.api_token
  region = "fr-par"
  zone = "fr-par-1"
  version = "~> 1.11"
}

// IPs
resource "scaleway_instance_ip" "main_server" {}
resource "scaleway_instance_ip" "huey_1" {}
//resource "scaleway_instance_ip" "huey_2" {}
//resource "scaleway_instance_ip" "huey_3" {}

// SERVERS
resource "scaleway_instance_server" "main_server" {
  name = "eve-shekel--main-server"
  image = var.main_server_image
  type = "DEV1-S"
  ip_id = scaleway_instance_ip.main_server.id
  security_group_id = scaleway_instance_security_group.main_server.id
  placement_group_id = scaleway_instance_placement_group.eve_shekel.id
}
resource "scaleway_instance_server" "huey_1" {
  name = "eve-shekel--huey-1"
  image = var.huey_image
  type = "DEV1-S"
  ip_id = scaleway_instance_ip.huey_1.id
  security_group_id = scaleway_instance_security_group.huey.id
  placement_group_id = scaleway_instance_placement_group.eve_shekel.id
}
//resource "scaleway_instance_server" "huey_2" {
//  name = "eve-shekel--huey-2"
//  image = var.huey_image
//  type = "DEV1-S"
//  ip_id = scaleway_instance_ip.huey_2.id
//  security_group_id = scaleway_instance_security_group.huey.id
//  placement_group_id = scaleway_instance_placement_group.eve_shekel.id
//}
//resource "scaleway_instance_server" "huey_3" {
//  name = "eve-shekel--huey-3"
//  image = var.huey_image
//  type = "DEV1-S"
//  ip_id = scaleway_instance_ip.huey_3.id
//  security_group_id = scaleway_instance_security_group.huey.id
//  placement_group_id = scaleway_instance_placement_group.eve_shekel.id
//}

// PLACEMENT GROUPS
resource "scaleway_instance_placement_group" "eve_shekel" {
  name = "eve-shekel"
  policy_type = "low_latency"
  policy_mode = "optional"
}

// SECURITY GROUPS
//// MAIN SERVER
resource "scaleway_instance_security_group" "main_server" {
  name = "shekel main server"
  description = "Allows redis and proxy connections from other servers"
  inbound_default_policy = "drop"
  outbound_default_policy = "accept"

  inbound_rule {
    action = "accept"
    ip_range = "0.0.0.0/0"
    port = 22
  }
  inbound_rule {
    action = "accept"
    ip = "51.158.77.11"
    port = 8080
  }
  inbound_rule {
    action = "accept"
    ip = scaleway_instance_ip.huey_1.address
    port = "6379"
  }
//  inbound_rule {
//    action = "accept"
//    ip = scaleway_instance_ip.huey_2.address
//    port = "6379"
//  }
//  inbound_rule {
//    action = "accept"
//    ip = scaleway_instance_ip.huey_3.address
//    port = "6379"
//  }
}

//// HUEY
resource "scaleway_instance_security_group" "huey" {
  name = "shakel huey"
  description = "Blocks inbound traffic"
  inbound_default_policy = "accept" # not sure why huey can't connect to redis when dropping inbound traffic
  outbound_default_policy = "accept"

  inbound_rule {
    action = "accept"
    ip_range = "0.0.0.0/0"
    port = 22
  }
}
