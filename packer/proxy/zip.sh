#!/usr/bin/env bash

mkdir dist || true
tar --exclude-vcs --exclude-vcs-ignores \
    -zcvf \
    -C packer/files/ \
    dist/eve-shekel-proxy.tar.gz \
    caddy.service caddyfile run_caddy.sh
