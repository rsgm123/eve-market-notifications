#!/usr/bin/env bash


sudo mkdir /proxy
sudo tar -xf /tmp/eve-shekel-proxy.tar.gz -C /proxy


# install dependencies
cd /tmp

sudo apt-get update
sudo apt-get install -y less htop

curl https://getcaddy.com | bash -s personal
sudo mkdir /var/log/caddy

# setup systemd services
sudo mv /proxy/caddy.service /lib/systemd/system/
sudo systemctl enable caddy.service
