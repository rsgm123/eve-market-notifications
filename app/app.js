import 'vuetify/dist/vuetify.min.css'

import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Vuetify from 'vuetify'

import routes from './routes.js'
import vuexStore from './store.js'

import Navbar from './components/Navbar.vue'
import Snackbar from './components/Snackbar.vue'

import colors from 'vuetify/es5/util/colors'
import websockets from "./plugins/websockets";

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(websockets);

Vue.use(Vuetify, {
  theme: {
    primary: colors.grey.darken2,
    secondary: colors.grey.darken3,
    accent: colors.grey.lighten1,

    info: colors.blue.darken3,
    success: colors.green.darken3,
    warning: colors.yellow.darken3,
    error: colors.red.darken3,
  }
});


const store = new Vuex.Store(
    vuexStore
);


const router = new VueRouter({
  mode: 'history',
  routes
});


router.beforeEach((to, from, next) => {
  if (to.matched.some(route => !route.meta.public)) {
    if (!store.getters.loggedIn) {
      next({
        name: 'landing'
      });
    } else {
      next()
    }

  } else if (to.matched.length === 0) {
    next({name: 'dashboard'});

  } else {
    next()
  }
});


store.dispatch('authenticate').then(() => {

  const app = new Vue({
    router,
    store,

    components: {
      Navbar,
      Snackbar,
    },
  }).$mount('#app');

});
