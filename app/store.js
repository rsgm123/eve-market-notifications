import Vue from 'vue'
import _ from 'lodash'
import {AXIOS} from "./constants";


export default {
  strict: true,

  state: {
    characters: {},
    orders: {},
    alerts: {},

    snackbar: false,
  },

  mutations: {
    addCharacter(state, character) {
      Vue.set(state.characters, character.id, {
        orders: [],
        alerts: [],
        ...character,
      })
    },

    updateOrders(state, {character, orders}) {
      orders.forEach(order => Vue.set(state.orders, order.order_id, order));
      state.characters[character].orders = orders.map(order => order.order_id)
    },

    updateAlerts(state, {character, alerts}) {
      alerts.forEach(alert => Vue.set(state.alerts, alert.id, alert));
      state.characters[character].alerts = alerts.map(alert => alert.id)
    },

    clearAlert(state, alert) {
      alert.cleared = true
    },

    setSnackbar(state, snackbar) {
      state.snackbar = snackbar
    },
  },

  actions: {
    authenticate({dispatch, commit}) {
      return AXIOS.get('/api/characters/').then(response => {
        if (response.data.length > 0) {
          response.data.forEach(character => {
            dispatch('addCharacter', character);
          });
        }
      })
    },

    addCharacter({dispatch, commit}, character) {
      commit('addCharacter', character);

      Vue.sockets.listener(`/character/${character.id}/`, () => dispatch('updateAllCharacters'));

      // update orders twice, right away, then after a character update in a few seconds
      dispatch('updateCharacter', character.id)
    },

    // debounced function for socket character update messages, since we do this for every order update
    updateAllCharacters: _.debounce(({state, dispatch, commit}) => {
      console.log('Updating all characters');
      Object.values(state.characters).forEach(character => {
        dispatch('updateCharacter', character.id)
      })
    }, 10000), // wait 10 seconds


    updateCharacter({commit}, characterId) { // called by socket action
      AXIOS.get(`/api/orders/?character=${characterId}`).then(response => {
        commit('updateOrders', {character: characterId, orders: response.data});
      }).catch(error => {
        commit('setSnackbar', 'Error fetching orders, try reloading the page.')
      });

      AXIOS.get(`/api/alerts/?order__character=${characterId}`).then(response => {
        commit('updateAlerts', {character: characterId, alerts: response.data});
      }).catch(error => {
        commit('setSnackbar', 'Error fetching alerts, try reloading the page.')
      })
    },

    clearAlert({commit}, alert) {
      AXIOS.patch(`/api/alerts/${alert.id}/`, {cleared: true}).then(response => {
        commit('clearAlert', alert)
      }).catch(error => {
        commit('setSnackbar', 'Error clearing alert')
      })
    },
  },

  getters: {
    characterList: (state) => Object.values(state.characters),

    loggedIn: (state, getters) => getters.characterList.length > 0,

    characterOrders: (state, getters) => // object with characterId -> list of orders
        _.fromPairs(getters.characterList.map(c => [c.id, c.orders.map(order => state.orders[order])])),
    characterAlerts: (state, getters) => // object with characterId -> list of alerts
        _.fromPairs(getters.characterList.map(c => [c.id, c.alerts.map(alert => state.alerts[alert])])),

    orders: (state) => Object.values(state.orders),
    currentOrders: (state, getters) => getters.orders.filter(order => order.is_open),

    alerts: (state) => Object.values(state.alerts),
  },
}
