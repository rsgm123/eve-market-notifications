import Landing from './views/Landing.vue'
import Dashboard from './views/Dashboard.vue'
import Orders from './views/Orders.vue'
import CharacterDashboard from './views/CharacterDashboard.vue'
import Market from './views/Market.vue'

import Auth from './views/Auth.vue'
import AuthRedirect from './views/AuthRedirect.vue'


export default [
  // landing page
  {
    path: '/',
    name: 'landing',
    component: Landing,
    meta: {'public': true},
  },

  // auth routes
  {
    path: '/add-character',
    name: 'auth',
    component: Auth,
    meta: {'public': true},
  },
  {
    path: '/add-character/redirect',
    name: 'authRedirect',
    component: AuthRedirect,
    meta: {'public': true},
  },

  // market browser
  {
    path: '/market/:itemId',
    name: 'market',
    component: Market,
    meta: {'public': true},
    props: true,
  },


  // logged in routes
  {
    path: '/home',
    name: 'dashboard',
    component: Dashboard,
  },

  {
    path: '/orders',
    name: 'orders',
    component: Orders,
  },

  {
    path: '/characters/:characterId',
    name: 'characterDashboard',
    component: CharacterDashboard,
    props: true,
  },
]
