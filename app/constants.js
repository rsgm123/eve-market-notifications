import axios from 'axios'


// set axios config
export const AXIOS = axios.create({
  timeout: 10000,
});

export const ALERT_CHANGE = {
  closed: {icon: 'close', color: 'red'},
  expired: {icon: 'hourglass_empty', color: 'red'},
  price: {icon: 'attach_money', color: 'orange'},
  volume: {icon: 'trending_down', color: 'amber'},
  time: {icon: 'alarm', color: 'amber'},
};