random_page_cost: 4 -> 8
work_mem: 4MB -> 8MB (8000KB)


SET (https://www.postgresql.org/docs/8.3/sql-set.html)
shared_buffers: 128MB -> 425MB
