EVE Market Notifications
===

EVE Market Notifications, formerly EVE Shekel, watches the public EVE online item markets and tracks order changes.
The main purpose of this app is to notify market traders when their orders filled or get outbid.

The secondary purpose is collecting and cateloging market data.
EVE online has about 1.3m public market orders on average.
All orders are updated every 5 minutes to capitalize on the EVE API cache times.

In production this is deployed as a set of scalable microservices,
 - reverse proxy
 - django server
 - greenlet huey workers
 - threaded huey workers
 - database
 