FAQ
===


## Why does this app require ESI permissions?

Eve shekel requires the `read_character_orders` permission to read market orders that your character has made.
Without this permission, there is no way to tell which market order belongs to which character.
Similarly, it would not be possible to tell when an order gets completed.

This is the only permission that this app needs to function, everything else this relies on is public information.
This includes public ESI endpoints to get region and item information, as well as character name and ID.

Optional permissions are for your convenience.


## Why should I trust you and give you permission for my data?

In general you should not trust third party applications with ESI permissions.
Some ESI scopes give access to a lot of very dangerous data.

Eve shekel only needs permission to read market orders, so that is the only scope that is requested.
This app does not keep any more information on market orders than needed to work.

The code is all opensource, hosted at https://gitlab.com/rsgm123/eve-shekel.
Feel free to review the code, or even request changes as you see fit.

